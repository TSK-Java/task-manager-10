package ru.tsc.kirillov.tm.api.service;

import ru.tsc.kirillov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    void remove(Project project);

    void clear();

}
