package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.IProjectController;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjectList() {
        System.out.println("[Список проектов]");
        final List<Project> projects = projectService.findAll();
        for(final Project project: projects) {
            if (project == null)
                continue;
            System.out.println(project);
        }
        System.out.println("[Конец списка]");
    }

    @Override
    public void clearProject() {
        System.out.println("[Очистка списка проектов]");
        projectService.clear();
        System.out.println("[Список успешно очищен]");
    }

    @Override
    public void createProject() {
        System.out.println("[Создание проекта]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null)
            System.out.println("[Ошибка при создании проекта]");
        else
            System.out.println("[Проект успешно создан]");
    }

}
