package ru.tsc.kirillov.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProject();

    void createProject();

}
