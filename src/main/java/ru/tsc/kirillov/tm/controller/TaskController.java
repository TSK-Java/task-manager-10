package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.ITaskController;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[Список задач]");
        final List<Task> tasks = taskService.findAll();
        for(final Task task: tasks) {
            if (task == null)
                continue;
            System.out.println(task);
        }
        System.out.println("[Конец списка]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[Очистка списка задач]");
        taskService.clear();
        System.out.println("[Список успешно очищен]");
    }

    @Override
    public void createTask() {
        System.out.println("[Создание задачи]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.create(name, description);
        if (task == null)
            System.out.println("[Ошибка при создании задачи]");
        else
            System.out.println("[Задача успешно создана]");
    }

}
